<?php

    // require('animal.php');
    require_once('frog.php');
    require_once('ape.php');

    $sheep = new animal("shaun");

    echo "Name : " . $sheep->name . "<br>";
    echo "legs : " . $sheep->legs . "<br>";
    echo "cold blooded : " . $sheep->cold_blooded . "<br> <br>";

    $frog = new frog("buduk");

    echo "Name : " . $frog->name . "<br>";
    echo "legs : " . $frog->legs ."<br>";
    echo "cold blooded : " . $frog->cold_blooded . "<br>";
    $frog->jump();

    echo "<br>";

    $ape = new ape("kera sakti");

    echo "Name : " . $ape->name . "<br>";
    echo "legs : " . $ape->legs . "<br>";
    echo "cold blooded : " . $ape->cold_blooded . "<br>";
    $ape->yell();

    
?>