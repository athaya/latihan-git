<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\CastController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [HomeController::class, 'utama']);
Route::get('/register', [AuthController::class, 'bio']);
Route::post('/welcome', [AuthController::class, 'welcome']);

Route::get('/table', function(){
    return view ('halaman.table');
});

Route::get('/data-table' , function(){
    return view ('halaman.data-table');
});

// CRUD Kategori

// Create Data

// Route untuk mengarah ke form tambah cast
Route::get('/cast/create', [CastController::class, 'create']);

// Route untuk menyimpan data inputan ke db
Route::post('/cast', [CastController::class, 'store']);

// Read
// menampilkan semua data yang ada di db
Route::get('/cast', [CastController::class, 'index']);

// detail cast berdasarkan id
Route::get('/cast/{cast_id}', [CastController::class, 'show']);

// update
// route yang mengarah ke form update berdasarkan id
Route::get('/cast/{cast_id}/edit', [CastController::class, 'edit']);
// untuk update data berdasarkan id di db
Route::put('/cast/{cast_id}', [CastController::class, 'update']);

// delete
Route::delete('/cast/{cast_id}', [CastController::class, 'destroy']);