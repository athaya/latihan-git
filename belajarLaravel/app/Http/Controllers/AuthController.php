<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function bio()
    {
        return view('halaman.register');
    }

    public function welcome(Request $request)
    {
        $Fullname = $request['fname'] . " " . $request['lname'];
        $jenisKelamin = $request['M'];

        return view ('halaman.home', ['Fullname' => $Fullname , 'jenisKelamin'=> $jenisKelamin]);
    }
}
