@extends('layout.master')
@section('title')
    Halaman Edit Cast
@endsection

@section('content')

<form action="/cast/{{$cast->id}}" method="POST">
  @csrf
  @method('put')
    <div class="form-group">
      <label>Nama Cast</label>
      <textarea name="nama" type="text" class="form-control">{{old('nama', $cast->nama)}}</textarea>
    </div>
      @error('nama')
      <div class="alert alert-danger">{{ $message }}</div>
      @enderror
      <div class="form-group">
        <label>Umur Cast</label>
        <textarea name="umur" type="integer" class="form-control">{{old('umur', $cast->umur)}}</textarea>
      </div>
        @error('umur')
        <div class="alert alert-danger">{{ $message }}</div>
        @enderror
    <div class="form-group">
      <label>Bio Cast</label>
      <textarea name="bio" class="form-control" cols="30" rows="10">{{old('bio', $cast->bio)}}</textarea>
    </div>
      @error('bio')
      <div class="alert alert-danger">{{ $message }}</div>
      @enderror
    <button type="submit" class="btn btn-primary">Submit</button>
  </form>

@endsection