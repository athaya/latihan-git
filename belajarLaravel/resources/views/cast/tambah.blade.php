@extends('layout.master')
@section('title')
    Halaman Tambah Cast
@endsection

@section('content')

<form action="/cast" method="POST">
  @csrf
    <div class="form-group">
      <label>Nama Cast</label>
      <textarea name="nama" type="text" value="{{('nama')}}" class="form-control"></textarea>
    </div>
      @error('nama')
      <div class="alert alert-danger">{{ $message }}</div>
      @enderror
      <div class="form-group">
        <label>Umur Cast</label>
        <textarea name="umur" type="integer" value="{{('umur')}}" class="form-control"></textarea>
      </div>
        @error('umur')
        <div class="alert alert-danger">{{ $message }}</div>
        @enderror
    <div class="form-group">
      <label>Bio Cast</label>
      <textarea name="bio" value="{{('bio')}}" class="form-control" cols="30" rows="10"></textarea>
    </div>
      @error('bio')
      <div class="alert alert-danger">{{ $message }}</div>
      @enderror
    <button type="submit" class="btn btn-primary">Submit</button>
  </form>

@endsection