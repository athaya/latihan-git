@extends('layout.master')
@section('title')
    Buat Account Baru
@endsection

@section('content')
    
<h3>Sign Up Form</h3> <br>

    <form action="/welcome" method="POST">
        @csrf

        <label>First Name</label> <br>
        <input type="text" name=fname required> <br>
    
        <label>Last Name</label> <br>
        <input type="text" name=lname> <br> <br>

        <label> Gender</label> <br>
        <input type="radio" name="M" value="1"> Male <br>
        <input type="radio" name="M" value="2"> Female <br>
        <input type="radio" name="M" value="3"> Other <br> <br>

        <label>Nationality:</label> <br>
        <select name="bahasa" id="">
            <option value="">Indonesian</option>
            <option value="">Malaysian</option>
            <option value="">Singaporean</option>
        </select> <br> <br>

        <label>Language Spoken</label> <br>
        <input type="checkbox" value="1" name="Language Spoken"> Bahasa Indonesia <br>
        <input type="checkbox" value="1" name="Language Spoken"> English <br>
        <input type="checkbox" value="1" name="Language Spoken"> Others <br> <br>

        <label>Bio</label> <br>
        <textarea name="Bio" cols="30" rows="10"></textarea> <br>

        <input type="submit" value="Sign Up">


    </form>

@endsection