@extends('layout.master')
@section('title')
    Pendaftaran Akun Berhasil
@endsection

@section('content')

    <h1>SELAMAT DATANG! {{$Fullname}} </h1>
    <h2>Jenis Kelamin = 
        @if ($jenisKelamin === "1")
            Laki-Laki
        @else
            Perempuan
        @endif
    </h2>
    <h2>Terima kasih telah bergabung di Sanberbook. Social Media kita bersama!</h2>

@endsection